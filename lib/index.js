"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.handler = exports.init = void 0;
const assetworxsdk_1 = require("assetworxsdk");
const axios_1 = __importDefault(require("axios"));
let logger;
let bucket;
/**
 * 1. Initializes connection to the logger
 * 2. Initializes connection to the S3 bucket
 * 3. Starts handler, providing the payload and response object
 *
 * @export
 * @param {Request} req
 * @return {*}  {Promise<any[]>}
 */
function init(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const environment = req.body.environment;
        const log_id = req.body.log_id;
        logger = logger || new assetworxsdk_1.Logger({ environment, log_id });
        logger.log({ message: `Started app`, status: assetworxsdk_1.LogStatus.START });
        bucket = bucket || new assetworxsdk_1.S3(environment, logger);
        logger.log({ message: `Running handler`, status: assetworxsdk_1.LogStatus.RUNNING });
        exports.handler(req, res);
    });
}
exports.init = init;
const handler = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { metric, file } = req.body;
        const buffer = yield bucket.downloadFile(file);
        if (buffer) {
            const locations = buffer.toString().split(',');
            const predictions = yield Promise.all(locations.map((location) => __awaiter(void 0, void 0, void 0, function* () {
                logger.log({ message: `Retrieving latitude and longitude for ${location}...`, status: assetworxsdk_1.LogStatus.RUNNING });
                const options = yield (yield axios_1.default.get(`https://nominatim.openstreetmap.org/search?city=${location}&format=json`)).data;
                const option = options[0];
                const { lat, lon } = option;
                logger.log({ message: `Retrieving weather information for ${location}...`, status: assetworxsdk_1.LogStatus.RUNNING });
                const res = yield (yield axios_1.default.get(`https://www.7timer.info/bin/astro.php?lon=${lon}&lat=${lat}&ac=0&unit=metric&output=json&tzshift=0`)).data;
                const dataseries = res.dataseries;
                if (metric == 'F') {
                    dataseries.forEach(data => {
                        data.temp2m = data.temp2m * 9 / 5 + 32;
                    });
                }
                logger.log({ message: `Storing weather information for ${location}...`, status: assetworxsdk_1.LogStatus.RUNNING });
                return { location, dataseries };
            })));
            logger.log({ message: `Completed, sending back data!`, status: assetworxsdk_1.LogStatus.FINISH });
            // Result
            return res.status(200).send({
                predictions
            });
        }
        else {
            throw { statusCode: '400', message: 'Errorrrr' };
        }
    }
    catch (err) {
        logger.log({ message: `Error: ${err}`, status: assetworxsdk_1.LogStatus.ERROR });
        return res.status(err === null || err === void 0 ? void 0 : err.statusCode).send({ message: err === null || err === void 0 ? void 0 : err.message, statusCode: err === null || err === void 0 ? void 0 : err.statusCode });
    }
});
exports.handler = handler;
exports.default = exports.handler;
