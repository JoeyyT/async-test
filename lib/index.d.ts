import { Request, Response } from 'express';
/**
 * 1. Initializes connection to the logger
 * 2. Initializes connection to the S3 bucket
 * 3. Starts handler, providing the payload and response object
 *
 * @export
 * @param {Request} req
 * @return {*}  {Promise<any[]>}
 */
export declare function init(req: Request, res: Response): Promise<void>;
export declare const handler: (req: any, res: any) => Promise<any>;
export default handler;
//# sourceMappingURL=index.d.ts.map