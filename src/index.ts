import { Request, Response } from 'express';
import { Logger, LogStatus, S3 } from 'assetworxsdk';
import axios from 'axios';

let logger: Logger;
let bucket: S3;

/**
 * 1. Initializes connection to the logger
 * 2. Initializes connection to the S3 bucket
 * 3. Starts handler, providing the payload and response object
 *
 * @export
 * @param {Request} req
 * @return {*}  {Promise<any[]>}
 */
export async function init(req: Request, res: Response): Promise<void> {
    const environment: string = req.body.environment;
    const log_id: number = req.body.log_id;
    logger = logger || new Logger({environment, log_id})
    logger.log({message: `Started app`, status: LogStatus.START})
    bucket = bucket || new S3(environment, logger);
    logger.log({message: `Running handler`, status: LogStatus.RUNNING})
    handler(req, res);
}

export const handler = async(req: Request, res: Response): Promise<any> => {
    try {
        const {metric, file} = req.body;

        const buffer: Buffer = await bucket.downloadFile(file) as Buffer;

        if (buffer) {
            const locations = buffer.toString().split(',');

            const predictions = await Promise.all(locations.map(async (location) => {
                logger.log({message: `Retrieving latitude and longitude for ${location}...`, status: LogStatus.RUNNING})
                const options = await (await axios.get(`https://nominatim.openstreetmap.org/search?city=${location}&format=json`)).data;
                const option = options[0];
                const {lat, lon} = option;
                logger.log({message: `Retrieving weather information for ${location}...`, status: LogStatus.RUNNING})
                const res = await (await axios.get(`https://www.7timer.info/bin/astro.php?lon=${lon}&lat=${lat}&ac=0&unit=metric&output=json&tzshift=0`)).data
                const dataseries: any[] = res.dataseries
                if (metric == 'F') {
                    dataseries.forEach(data => {
                        data.temp2m = data.temp2m * 9 /5 + 32
                    })
                }
                logger.log({message: `Storing weather information for ${location}...`, status: LogStatus.RUNNING})
                return {location, dataseries} 
            }));

            logger.log({message: `Completed, sending back data!`, status: LogStatus.FINISH})

            // Result
            return res.status(200).send({
                predictions
            });
        } else {
            throw {statusCode: '400', message: 'Errorrrr'}
        }
    } catch(err) {
        logger.log({message: `Error: ${err}`, status: LogStatus.ERROR})
        return res.status(err?.statusCode).send({message: err?.message, statusCode: err?.statusCode});
    }
}
export default handler